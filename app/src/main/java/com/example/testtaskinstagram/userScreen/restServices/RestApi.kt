package com.example.testtaskinstagram.userScreen.restServices

import com.example.testtaskinstagram.userScreen.data.UserAccessToken
import com.example.testtaskinstagram.userScreen.data.UserMediaList
import retrofit2.Call
import retrofit2.http.*

interface RestApi {

    @FormUrlEncoded
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @POST("access_token")
    fun queryAccessToken(
        @Field("client_id") client_id: String,
        @Field("client_secret") client_secret: String,
        @Field("grant_type") grant_type: String,
        @Field("redirect_uri") redirect_uri: String,
        @Field("code") code: String
    ): Call<UserAccessToken>

    @GET("media")
    fun queryUserMedia(
        @Query("fields") fields: String,
        @Query("access_token") access_token: String
    ): Call<UserMediaList>

}