package com.example.testtaskinstagram.userScreen.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.testtaskinstagram.R
import com.example.testtaskinstagram.userScreen.data.UserMediaList
import com.squareup.picasso.Picasso

class UserMediaAdapter(
    private val photoClickListener: PhotoClickListener,
    private val itemHeight: Int
) :
    RecyclerView.Adapter<UserMediaAdapter.ViewHolder>() {

    interface PhotoClickListener {
        fun photoClicked(id: Long)
    }

    var userMediaList: UserMediaList = UserMediaList(emptyList())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflatedView =
            LayoutInflater.from(parent.context).inflate(R.layout.rv_preview_photo_item, null)
        val holder = ViewHolder(inflatedView)
        holder.ivPhoto.layoutParams.height = itemHeight
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.ivPhoto.setImageDrawable(null)
        Picasso.with(holder.ivPhoto.context).load(userMediaList.userMediaList[position].mediaUrl).into(holder.ivPhoto)
        holder.itemView.setOnClickListener(View.OnClickListener {
            photoClickListener.photoClicked(userMediaList.userMediaList[position].id)
        })
    }

    override fun getItemCount(): Int = userMediaList.userMediaList.size

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var ivPhoto: ImageView = v.findViewById(R.id.ivPhoto)
    }
}