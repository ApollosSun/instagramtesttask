package com.example.testtaskinstagram.userScreen.presenter

import android.content.Context
import com.example.testtaskinstagram.R
import com.example.testtaskinstagram.userScreen.data.UserMediaItem
import com.example.testtaskinstagram.userScreen.data.UserMediaList
import com.example.testtaskinstagram.userScreen.interfaces.IUserMediaLoading
import com.example.testtaskinstagram.userScreen.restServices.RestApiService
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object UserMediaPresenter {

    private val apiService = RestApiService()
    lateinit var loadingListener: IUserMediaLoading

    var userMediaList: UserMediaList = UserMediaList(emptyList())
    var shownPostId: Long = 0

    fun initPresenter(loadingListener: IUserMediaLoading) {
        this.loadingListener = loadingListener
    }

    fun loadUserMedia(context: Context, accessCode: String) {
        val clientId = context.getString(R.string.client_id)
        val clientSecret = context.getString(R.string.client_secret)
        val redirectUri = context.getString(R.string.redirect_uri)

        apiService.queryAccessToken(
            clientId,
            clientSecret,
            "authorization_code",
            redirectUri,
            accessCode
        ) {
            val accessToken = it?.accessToken
            if (accessToken != null) {
                queryMedia(accessToken)
            }
        }
    }

    fun queryMedia(accessToken: String) {
        apiService.queryUserMedia("id,caption,media_url,username,timestamp", accessToken) {
            if (it != null) {
                userMediaList = it
                loadingListener.userMediaLoaded()
            }
        }
    }

    fun getConvertedTime(timestamp: String): String {
        var formatter: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        val date: Date = formatter.parse(timestamp)

        formatter = SimpleDateFormat("MMMM dd, yyyy")
        val formattedDate = formatter.format(date)

        return formattedDate
    }

    fun getShowPost(): UserMediaItem? {
        return userMediaList.userMediaList.find { it.id == shownPostId }
    }

}