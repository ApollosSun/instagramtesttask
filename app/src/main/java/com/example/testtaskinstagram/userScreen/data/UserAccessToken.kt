package com.example.testtaskinstagram.userScreen.data

import com.google.gson.annotations.SerializedName

data class UserAccessToken(
    @SerializedName("access_token") val accessToken: String,
    @SerializedName("user_id") val userId: Long
)