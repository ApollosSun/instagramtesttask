package com.example.testtaskinstagram.userScreen.restServices

import com.example.testtaskinstagram.userScreen.data.UserMediaList
import com.example.testtaskinstagram.userScreen.data.UserAccessToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RestApiService {

    fun queryAccessToken(
        client_id: String,
        client_secret: String,
        grant_type: String,
        redirect_uri: String,
        code: String, onResult: (UserAccessToken?) -> Unit
    ) {
        val retrofit =
            ServiceBuilder().buildService("https://api.instagram.com/oauth/", RestApi::class.java)
        retrofit.queryAccessToken(client_id, client_secret, grant_type, redirect_uri, code)
            .enqueue(object : Callback<UserAccessToken> {
                override fun onFailure(call: Call<UserAccessToken>, t: Throwable) {
                    onResult(null)
                }

                override fun onResponse(
                    call: Call<UserAccessToken>,
                    response: Response<UserAccessToken>
                ) {
                    val userAccessToken = response.body()
                    onResult(userAccessToken)
                }
            }
            )
    }

    fun queryUserMedia(
        fields: String,
        access_token: String, onResult: (UserMediaList?) -> Unit
    ) {
        val retrofit =
            ServiceBuilder().buildService("https://graph.instagram.com/me/", RestApi::class.java)
        retrofit.queryUserMedia(fields, access_token)
            .enqueue(object : Callback<UserMediaList> {
                override fun onFailure(call: Call<UserMediaList>, t: Throwable) {
                    onResult(null)
                }

                override fun onResponse(call: Call<UserMediaList>, response: Response<UserMediaList>) {
                    val userMediaList = response.body()
                    onResult(userMediaList)
                }
            }
            )
    }

}