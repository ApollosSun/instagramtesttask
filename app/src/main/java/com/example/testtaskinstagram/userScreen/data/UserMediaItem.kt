package com.example.testtaskinstagram.userScreen.data

import com.google.gson.annotations.SerializedName

class UserMediaItem(
    @SerializedName("id") val id: Long,
    @SerializedName("caption") val caption: String,
    @SerializedName("media_url") val mediaUrl: String,
    @SerializedName("username") val username: String,
    @SerializedName("timestamp") val timestamp: String
)