package com.example.testtaskinstagram.userScreen.ui

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testtaskinstagram.R
import com.example.testtaskinstagram.userScreen.interfaces.IUserMediaLoading
import com.example.testtaskinstagram.userScreen.presenter.UserMediaPresenter

class UserActivity : AppCompatActivity(), IUserMediaLoading, UserMediaAdapter.PhotoClickListener {

    private val userPostFragment: UserPostFragment = UserPostFragment()
    lateinit var rvMedia: RecyclerView
    lateinit var userMediaAdapter: UserMediaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        UserMediaPresenter.initPresenter(this)

        val accessCode: String? = intent.extras?.getString("accessCode")
        val accessToken: String? = intent.extras?.getString("token")

        if (accessCode != null) {
            UserMediaPresenter.loadUserMedia(applicationContext, accessCode)
        } else {
            UserMediaPresenter.queryMedia(accessToken!!)
        }

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val screenWidth = displayMetrics.widthPixels
        userMediaAdapter = UserMediaAdapter(this, screenWidth / 3)

        rvMedia = findViewById(R.id.rvMedia)
        rvMedia.layoutManager = GridLayoutManager(this, 3)
        rvMedia.adapter = userMediaAdapter
        rvMedia.setHasFixedSize(true)
    }

    override fun userMediaLoaded() {
        userMediaAdapter.userMediaList = UserMediaPresenter.userMediaList
        userMediaAdapter.notifyDataSetChanged()
    }

    override fun photoClicked(id: Long) {
        UserMediaPresenter.shownPostId = id

        supportFragmentManager.beginTransaction()
            .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
            .add(R.id.container, userPostFragment, "userPostFragment")
            .commit()

        rvMedia.visibility = View.GONE
    }

    override fun onBackPressed() {
        if (supportFragmentManager.findFragmentByTag("userPostFragment") == null) {
            super.onBackPressed()
        } else {
            UserMediaPresenter.shownPostId = 0

            supportFragmentManager.beginTransaction()
                .remove(userPostFragment)
                .commit()

            rvMedia.visibility = View.VISIBLE
        }
    }
}