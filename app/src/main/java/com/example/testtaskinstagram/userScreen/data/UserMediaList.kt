package com.example.testtaskinstagram.userScreen.data

import com.example.testtaskinstagram.userScreen.data.UserMediaItem
import com.google.gson.annotations.SerializedName

data class UserMediaList(
    @SerializedName("data") val userMediaList: List<UserMediaItem>
)