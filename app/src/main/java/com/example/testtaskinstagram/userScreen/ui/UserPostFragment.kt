package com.example.testtaskinstagram.userScreen.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.testtaskinstagram.R
import com.example.testtaskinstagram.userScreen.data.UserMediaItem
import com.example.testtaskinstagram.userScreen.presenter.UserMediaPresenter
import com.squareup.picasso.Picasso

class UserPostFragment : Fragment() {

    private lateinit var ivPhoto: ImageView
    private lateinit var tvUserName: TextView
    private lateinit var tvCaption: TextView
    private lateinit var tvTime: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_user_post, container, false)
        ivPhoto = view.findViewById(R.id.ivPhoto)
        tvUserName = view.findViewById(R.id.tvUserName)
        tvCaption = view.findViewById(R.id.tvCaption)
        tvTime = view.findViewById(R.id.tvTime)
        return view
    }

    override fun onResume() {
        super.onResume()
        updateInfo()
    }

    private fun updateInfo() {
        val post: UserMediaItem = UserMediaPresenter.getShowPost()!!
        Picasso.with(activity).load(post.mediaUrl).into(ivPhoto)
        tvUserName.text = post.username
        tvCaption.text = post.caption
        tvTime.text = UserMediaPresenter.getConvertedTime(post.timestamp.toString())
    }

}