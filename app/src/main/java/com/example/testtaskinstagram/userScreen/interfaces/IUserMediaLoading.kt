package com.example.testtaskinstagram.userScreen.interfaces

import com.example.testtaskinstagram.userScreen.data.UserMediaList

interface IUserMediaLoading {
    fun userMediaLoaded()
}