package com.example.testtaskinstagram.loginScreen.ui

import android.content.Intent
import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.example.testtaskinstagram.R
import com.example.testtaskinstagram.loginScreen.interfaces.IAuthorizationListener
import com.example.testtaskinstagram.loginScreen.tools.WebClient
import com.example.testtaskinstagram.userScreen.ui.UserActivity

class LogInActivity : AppCompatActivity(),
    IAuthorizationListener {

    private val authorizationUrl =
        "https://api.instagram.com/oauth/authorize?client_id=276238290295318&redirect_uri=https://apollossun.github.io/&scope=user_profile,user_media&response_type=code"
    private val companyDomain: String = "https://apollossun.github.io/?code="

    private lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
    }

    override fun onStart() {
        super.onStart()
        navigateToTestUserPage()
    }

    private fun showAuthorizationWindow() {
        webView = findViewById(R.id.webView)
        webView.loadUrl(authorizationUrl)
        webView.settings.javaScriptEnabled = true
        webView.webViewClient =
            WebClient(
                this,
                companyDomain
            )
    }

    override fun navigateToUserPage(accessCode: String) {
        val intent = Intent(this, UserActivity::class.java).apply {
            putExtra("accessCode", accessCode)
        }
        startActivity(intent)
    }

    private fun navigateToTestUserPage() {
        val accessToken =
            "IGQVJVcnAtMkQ1bjNPdUFfT2pXZAjFwNWJjc2hFX1BXNjgyb3hjOE1uZA0w5dkdwVFhieV92bU5EN0J0a2h6a2xSYlEyVHJockVDZAFhOSHlPN2pTMW9yZAXhORXo1aW9RTlp3dG9FTExkVlJJNzFzbmNHYwZDZD"
        val intent = Intent(this, UserActivity::class.java).apply {
            putExtra("token", accessToken)
        }
        startActivity(intent)
    }

}