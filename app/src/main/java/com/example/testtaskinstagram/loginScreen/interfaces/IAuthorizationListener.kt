package com.example.testtaskinstagram.loginScreen.interfaces

interface IAuthorizationListener {
    fun navigateToUserPage(accessCode: String)
}