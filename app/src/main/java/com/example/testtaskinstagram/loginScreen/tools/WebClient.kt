package com.example.testtaskinstagram.loginScreen.tools

import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.testtaskinstagram.loginScreen.interfaces.IAuthorizationListener

class WebClient constructor(
    private val authorizationListener: IAuthorizationListener,
    private val companyDomain: String
) : WebViewClient() {

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        val requestUrl = request?.url.toString()

        return if (companyDomain in requestUrl) {
            val accessCode = getAccessCodeFromUrl(requestUrl)
            authorizationListener.navigateToUserPage(accessCode)
            view?.stopLoading()
            false
        } else {
            view?.loadUrl(requestUrl)
            true
        }
    }

    private fun getAccessCodeFromUrl(url: String): String {
        val accessCode = url.substring(url.indexOf("=") + 1)
        return accessCode.substring(0, accessCode.indexOf("#_"))
    }

}